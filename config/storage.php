<?php

declare(strict_types=1);

namespace Smtm\Storage;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-storage')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-storage'
    );
    $dotenv->load();
}

$storageServices = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal(
        'SMTM_STORAGE_SERVICES',
        '{}'
    ),
    true,
    flags: JSON_THROW_ON_ERROR
);

return [
    'services' => $storageServices,
];
