<?php

declare(strict_types=1);

namespace Smtm\Storage;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Storage\Infrastructure\Service\Factory\StorageServiceFactory;
use Smtm\Storage\Infrastructure\Service\StorageService;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        InfrastructureServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                $infrastructureServicePluginManager = $callback();

                $infrastructureServicePluginManager->setFactory(
                    StorageService::class,
                    StorageServiceFactory::class
                );

                foreach ($container->get('config')['storage']['services'] as $storageServiceName => $storageServiceConfig) {
                    $infrastructureServicePluginManager->setFactory(
                        $storageServiceName,
                        function (
                            ContainerInterface $container,
                            $name,
                            array $options = null
                        ) use ($infrastructureServicePluginManager, $storageServiceConfig) {
                            return $infrastructureServicePluginManager->build(
                                StorageService::class,
                                $storageServiceConfig
                            );
                        }
                    );
                }

                return $infrastructureServicePluginManager;
            }
        ],
    ],
];
