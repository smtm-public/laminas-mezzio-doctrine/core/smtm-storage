<?php

namespace Smtm\Storage\Infrastructure\Service\Adapter;

use Aws\S3\S3ClientInterface;
use Smtm\Base\OptionsAwareInterface;
use Smtm\Base\OptionsAwareTrait;
use JetBrains\PhpStorm\ArrayShape;
use League\Flysystem\Config;
use League\Flysystem\DirectoryAttributes;
use League\Flysystem\FileAttributes;
use League\Flysystem\FilesystemAdapter;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\PathPrefixer;
use League\Flysystem\SymbolicLinkEncountered;
use League\Flysystem\UnableToCopyFile;
use League\Flysystem\UnableToCreateDirectory;
use League\Flysystem\UnableToDeleteDirectory;
use League\Flysystem\UnableToDeleteFile;
use League\Flysystem\UnableToMoveFile;
use League\Flysystem\UnableToReadFile;
use League\Flysystem\UnableToRetrieveMetadata;
use League\Flysystem\UnableToSetVisibility;
use League\Flysystem\UnableToWriteFile;
use Generator;
use League\Flysystem\UnixVisibility\PortableVisibilityConverter;
use League\Flysystem\UnixVisibility\VisibilityConverter;
use League\MimeTypeDetection\FinfoMimeTypeDetector;
use League\MimeTypeDetection\MimeTypeDetector;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class S3FileSystemAdapter implements FilesystemAdapter, OptionsAwareInterface
{

    use OptionsAwareTrait;

    protected PathPrefixer $prefixer;
    protected int $writeFlags;
    protected int $linkHandling;
    protected VisibilityConverter $visibility;
    protected MimeTypeDetector $mimeTypeDetector;

    public function __construct(
        protected S3ClientInterface $s3Client
    ) {
        $this->s3Client->registerStreamWrapper();

        $this->prefixer = new PathPrefixer('', DIRECTORY_SEPARATOR);
        $this->writeFlags = LOCK_EX;
        $this->linkHandling = LocalFilesystemAdapter::DISALLOW_LINKS;
        $this->visibility = new PortableVisibilityConverter();
        $this->mimeTypeDetector = new FinfoMimeTypeDetector();
    }

    public function write(string $path, string $contents, Config $config): void
    {
        $this->writeToFile($path, $contents, $config);
    }

    public function writeStream(string $path, $contents, Config $config): void
    {
        $this->writeToFile($path, $contents, $config);
    }

    /**
     * @param resource|string $contents
     */
    private function writeToFile(string $path, $contents, Config $config): void
    {
        $this->ensureDirectoryExists(
            dirname($path),
            $this->resolveDirectoryVisibility($config->get(Config::OPTION_DIRECTORY_VISIBILITY))
        );

        $bucketKey = $this->getBucketKey($path);

        try {
            $this->s3Client->putObject($bucketKey + ['Body' => $contents]);
        } catch (\Throwable $t) {
            throw UnableToWriteFile::atLocation($path, $t->getMessage());
        }

        if ($visibility = $config->get(Config::OPTION_VISIBILITY)) {
            $this->setVisibility($path, (string) $visibility);
        }
    }

    public function delete(string $path): void
    {
        $bucketKey = $this->getBucketKey($path);
        $object = $this->s3Client->getObject($bucketKey);

        if ( ! file_exists($path)) {
            return;
        }

        error_clear_last();

        if ( ! @unlink($location)) {
            throw UnableToDeleteFile::atLocation($path, error_get_last()['message'] ?? '');
        }
    }

    public function deleteDirectory(string $path): void
    {
        $bucketKey = $this->getBucketKey($path);
        $object = $this->s3Client->getObject($bucketKey);

        if ( ! $this->s3Client->getObject($path)) {
            return;
        }

        $contents = $this->listDirectoryRecursively($path, RecursiveIteratorIterator::CHILD_FIRST);

        /** @var SplFileInfo $file */
        foreach ($contents as $file) {
            if ( ! $this->deleteFileInfoObject($file)) {
                throw UnableToDeleteDirectory::atLocation($prefix, "Unable to delete file at " . $file->getPathname());
            }
        }

        unset($contents);

        if ( ! @rmdir($location)) {
            throw UnableToDeleteDirectory::atLocation($path, error_get_last()['message'] ?? '');
        }
    }

    private function listDirectoryRecursively(
        string $path,
        int $mode = \RecursiveIteratorIterator::SELF_FIRST
    ): Generator {
        $bucketKey = $this->getBucketKey($path);
        $listDirectories = $this->s3Client->listObjects($bucketKey);

        yield from new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS),
            $mode
        );
    }

    protected function deleteFileInfoObject(\SplFileInfo $file): bool
    {
        switch ($file->getType()) {
            case 'dir':
                return @rmdir((string) $file->getRealPath());
            case 'link':
                return @unlink((string) $file->getPathname());
            default:
                return @unlink((string) $file->getRealPath());
        }
    }

    public function listContents(string $path, bool $deep): iterable
    {
        $location = $this->prefixer->prefixPath($path);
        $bucketKey = $this->getBucketKey($path);
        $object = $this->s3Client->getObject($bucketKey);

        if ( ! is_dir($location)) {
            return;
        }

        /** @var SplFileInfo[] $iterator */
        $iterator = $deep ? $this->listDirectoryRecursively($location) : $this->listDirectory($location);

        foreach ($iterator as $fileInfo) {
            if ($fileInfo->isLink()) {
                if ($this->linkHandling & self::SKIP_LINKS) {
                    continue;
                }
                throw SymbolicLinkEncountered::atLocation($fileInfo->getPathname());
            }

            $path = $this->prefixer->stripPrefix($fileInfo->getPathname());
            $lastModified = $fileInfo->getMTime();
            $isDirectory = $fileInfo->isDir();
            $permissions = octdec(substr(sprintf('%o', $fileInfo->getPerms()), -4));
            $visibility = $isDirectory ? $this->visibility->inverseForDirectory($permissions) : $this->visibility->inverseForFile($permissions);

            yield $isDirectory ? new DirectoryAttributes($path, $visibility, $lastModified) : new FileAttributes(
                str_replace('\\', '/', $path),
                $fileInfo->getSize(),
                $visibility,
                $lastModified
            );
        }
    }

    public function move(string $source, string $destination, Config $config): void
    {
        $this->ensureDirectoryExists(
            dirname($source),
            $this->resolveDirectoryVisibility($config->get(Config::OPTION_DIRECTORY_VISIBILITY))
        );

        $bucketKey = $this->getBucketKey($source);
        $object = $this->s3Client->getObject($bucketKey);

        if ( ! @rename($source, $destination)) {
            throw UnableToMoveFile::fromLocationTo($source, $destination);
        }
    }

    public function copy(string $source, string $destination, Config $config): void
    {
        $this->ensureDirectoryExists(
            dirname($source),
            $this->resolveDirectoryVisibility($config->get(Config::OPTION_DIRECTORY_VISIBILITY))
        );

        if ( ! @copy($source, $destination)) {
            throw UnableToCopyFile::fromLocationTo($source, $destination);
        }
    }

    public function read(string $path): string
    {
        error_clear_last();

        $bucketKey = $this->getBucketKey($path);
        $object = $this->s3Client->getObject($bucketKey);

        $contents = @file_get_contents($path);

        if ($contents === false) {
            throw UnableToReadFile::fromLocation($path, error_get_last()['message'] ?? '');
        }

        return $contents;
    }

    public function readStream(string $path)
    {
        error_clear_last();

        $bucketKey = $this->getBucketKey($path);
        $object = $this->s3Client->getObject($bucketKey);

        $contents = @fopen($path, 'rb');

        if ($contents === false) {
            throw UnableToReadFile::fromLocation($path, error_get_last()['message'] ?? '');
        }

        return $contents;
    }

    protected function ensureDirectoryExists(string $dirname, int $visibility): void
    {
        return;

        $bucketKey = $this->getBucketKey($dirname);
        $objectExists = $this->s3Client->doesObjectExist($bucketKey['Bucket'], $bucketKey['Key']);

        if (!$objectExists) {
            $this->createDirectory($dirname, $this->s3Client->getConfig());
        }
    }

    public function fileExists(string $location): bool
    {
        $bucketKey = $this->getBucketKey($location);

        return $this->s3Client->doesObjectExist($bucketKey['Bucket'], $bucketKey['Key']);
    }

    public function createDirectory(string $path, Config $config): void
    {
        $bucketKey = $this->getBucketKey($path);

        $visibility = $config->get(Config::OPTION_VISIBILITY, $config->get(Config::OPTION_DIRECTORY_VISIBILITY));
        $permissions = $this->resolveDirectoryVisibility($visibility);

        try {
            $this->s3Client->putObject($bucketKey + ['Body' => '']);
        } catch (\Throwable $t) {
            throw UnableToCreateDirectory::atLocation($path, $t->getMessage());
        }
    }

    public function setVisibility(string $path, string $visibility): void
    {
        $bucketKey = $this->getBucketKey($path);
        $object = $this->s3Client->getObject($bucketKey);

        $visibility = is_dir($path) ? $this->visibility->forDirectory($visibility) : $this->visibility->forFile(
            $visibility
        );

        $this->setPermissions($path, $visibility);
    }

    public function visibility(string $path): FileAttributes
    {
        clearstatcache(false, $path);
        error_clear_last();

        $bucketKey = $this->getBucketKey($path);
        $object = $this->s3Client->getObject($bucketKey);

        $fileperms = @fileperms($path);

        if ($fileperms === false) {
            throw UnableToRetrieveMetadata::visibility($path, error_get_last()['message'] ?? '');
        }

        $permissions = $fileperms & 0777;
        $visibility = $this->visibility->inverseForFile($permissions);

        return new FileAttributes($path, null, $visibility);
    }

    private function resolveDirectoryVisibility(?string $visibility): int
    {
        return $visibility === null ? $this->visibility->defaultForDirectories() : $this->visibility->forDirectory(
            $visibility
        );
    }

    public function mimeType(string $path): FileAttributes
    {
        error_clear_last();

        $bucketKey = $this->getBucketKey($path);
        $object = $this->s3Client->getObject($bucketKey);

        $mimeType = $this->mimeTypeDetector->detectMimeTypeFromFile($path);

        if ($mimeType === null) {
            throw UnableToRetrieveMetadata::mimeType($path, error_get_last()['message'] ?? '');
        }

        return new FileAttributes($path, null, null, null, $mimeType);
    }

    public function lastModified(string $path): FileAttributes
    {
        error_clear_last();

        $bucketKey = $this->getBucketKey($path);
        $object = $this->s3Client->getObject($bucketKey);

        $lastModified = @filemtime($path);

        if ($lastModified === false) {
            throw UnableToRetrieveMetadata::lastModified($path, error_get_last()['message'] ?? '');
        }

        return new FileAttributes($path, null, null, $lastModified);
    }

    public function fileSize(string $path): FileAttributes
    {
        error_clear_last();

        $bucketKey = $this->getBucketKey($path);
        $object = $this->s3Client->getObject($bucketKey);

        if (is_file($path) && ($fileSize = @filesize($path)) !== false) {
            return new FileAttributes($path, $fileSize);
        }

        throw UnableToRetrieveMetadata::fileSize($path, error_get_last()['message'] ?? '');
    }

    #[ArrayShape(['Bucket' => 'string', 'Key' => 'string'])] protected function getBucketKey(string $path): array
    {
        $location = explode('/', $path);
        array_shift($location);
        $bucket = array_shift($location);
        $location = '/' . implode('/', $location);

        return [
            'Bucket' => $bucket,
            'Key' => $location,
        ];
    }

    private function listDirectory(string $location): Generator
    {
        $bucketKey = $this->getBucketKey($location);
        $object = $this->s3Client->getObject($bucketKey);

        $iterator = new DirectoryIterator($location);

        foreach ($iterator as $item) {
            if ($item->isDot()) {
                continue;
            }

            yield $item;
        }
    }

    private function setPermissions(string $location, int $visibility): void
    {
        error_clear_last();

        $bucketKey = $this->getBucketKey($location);
        $object = $this->s3Client->getObject($bucketKey);

        if ( ! @chmod($location, $visibility)) {
            $extraMessage = error_get_last()['message'] ?? '';
            throw UnableToSetVisibility::atLocation($location, $extraMessage);
        }
    }
}
