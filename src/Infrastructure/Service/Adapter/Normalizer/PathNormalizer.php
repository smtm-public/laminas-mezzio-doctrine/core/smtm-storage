<?php

namespace Smtm\Storage\Infrastructure\Service\Adapter\Normalizer;

use League\Flysystem\WhitespacePathNormalizer;

class PathNormalizer extends WhitespacePathNormalizer
{
    public function normalizePath(string $path): string
    {
        return str_replace('\\', '/', $path);
    }
}
