<?php

namespace Smtm\Storage\Infrastructure\Service\Adapter;

use Smtm\Base\OptionsAwareInterface;
use Smtm\Base\OptionsAwareTrait;
use League\Flysystem\Config;
use League\Flysystem\FileAttributes;
use League\Flysystem\FilesystemAdapter;
use League\Flysystem\Local\LocalFilesystemAdapter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LocalFileSystemAdapterDecorator implements FilesystemAdapter, OptionsAwareInterface
{

    use OptionsAwareTrait;

    protected \League\Flysystem\Local\LocalFilesystemAdapter $wrapped;

    public function __construct(array $options = [])
    {
        $this->options = $options;
        $this->wrapped = new LocalFilesystemAdapter(
            $options['location'] ?? null, // let it error out when no location has been provided
            null,
            $options['writeFlags'] ?? LOCK_EX,
            $options['linkHandling'] ?? LocalFilesystemAdapter::DISALLOW_LINKS,
            null
        );
    }

    public function move(string $source, string $destination, Config $config): void
    {
        $this->wrapped->move($source, $destination, $config);
    }

    public function read(string $path): string
    {
        return $this->wrapped->read($path);
    }

    public function visibility(string $path): FileAttributes
    {
        return $this->wrapped->visibility($path);
    }

    public function fileExists(string $path): bool
    {
        return $this->wrapped->fileExists($path);
    }

    public function mimeType(string $path): FileAttributes
    {
        return $this->wrapped->mimeType($path);
    }

    public function delete(string $path): void
    {
        $this->wrapped->delete($path);
    }

    public function setVisibility(string $path, string $visibility): void
    {
        $this->wrapped->setVisibility($path, $visibility);
    }

    public function listContents(string $path, bool $deep): iterable
    {
        return $this->wrapped->listContents($path, $deep);
    }

    public function write(string $path, string $contents, Config $config): void
    {
        $this->wrapped->write($path, $contents, $config);
    }

    public function writeStream(string $path, $contents, Config $config): void
    {
        $this->wrapped->writeStream($path, $contents, $config);
    }

    public function readStream(string $path)
    {
        return $this->wrapped->readStream($path);
    }

    public function deleteDirectory(string $path): void
    {
        $this->wrapped->deleteDirectory($path);
    }

    public function createDirectory(string $path, Config $config): void
    {
        $this->wrapped->createDirectory($path, $config);
    }

    public function lastModified(string $path): FileAttributes
    {
        return $this->wrapped->lastModified($path);
    }

    public function fileSize(string $path): FileAttributes
    {
        return $this->wrapped->fileSize($path);
    }

    public function copy(string $source, string $destination, Config $config): void
    {
        $this->wrapped->copy($source, $destination, $config);
    }
}
