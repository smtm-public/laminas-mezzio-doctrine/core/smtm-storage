<?php

declare(strict_types=1);

namespace Smtm\Storage\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait StorageServiceAwareTrait
{
    protected StorageService $storageService;

    public function getStorageService(): StorageService
    {
        return $this->storageService;
    }

    public function setStorageService(StorageService $storageService): static
    {
        $this->storageService = $storageService;

        return $this;
    }
}
