<?php

declare(strict_types=1);

namespace Smtm\Storage\Infrastructure\Service;

use Smtm\Base\Infrastructure\Collection\Collection;
use Smtm\Base\Infrastructure\Service\AbstractInfrastructureService;
use Smtm\Base\Infrastructure\Service\InfrastructureServiceInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Base\OptionsAwareInterface;
use Smtm\Base\OptionsAwareTrait;
use Smtm\Storage\Infrastructure\Service\Adapter\Normalizer\PathNormalizer;
use JetBrains\PhpStorm\Pure;
use Laminas\ServiceManager\Factory\FactoryInterface;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemAdapter;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class StorageService extends Filesystem implements InfrastructureServiceInterface, OptionsAwareInterface
{

    use OptionsAwareTrait;

    public function __construct(
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        FilesystemAdapter $adapter,
        array $config = [],
    ) {
        parent::__construct(
            $adapter,
            $config,
            new PathNormalizer()
        );
    }

    public function list($path): Collection
    {
        $directoryListing = $this->listContents($path);

        return new Collection(
            array_map(function ($element) {
                return [
                    'type' => $element->type(),
                    'path' => $element->path()
                ];
            }, $directoryListing->toArray())
        );
    }
}
