<?php

declare(strict_types=1);

namespace Smtm\Storage\Infrastructure\Service\Factory;

use Smtm\Base\Factory\ServiceNameAwareInterface;
use Smtm\Base\Factory\ServiceNameAwareTrait;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Storage\Infrastructure\Service\StorageService;
use Smtm\Storage\Infrastructure\Service\StorageServiceAwareInterface;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class StorageServiceAwareDelegator implements DelegatorFactoryInterface, ServiceNameAwareInterface
{

    use ServiceNameAwareTrait;

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var StorageServiceAwareInterface $object */
        $object = $callback();

        $object->setStorageService(
            $container
                ->get(InfrastructureServicePluginManager::class)
                ->get($options['name'] ?? $this->getServiceName() ?? StorageService::class)
        );

        return $object;
    }
}
