<?php

declare(strict_types=1);

namespace Smtm\Storage\Infrastructure\Service\Factory;

use Aws\S3\S3ClientInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Amazon\Infrastructure\Service\S3\S3Client;
use Smtm\Storage\Infrastructure\Service\Adapter\S3FileSystemAdapter;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use League\Flysystem\Local\LocalFilesystemAdapter;

/**
 * @author Angel Baev <angel@smtm.bg>
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class StorageServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $adapterName = $options['adapter']['name'] ?? LocalFilesystemAdapter::class;

        $adapter = null;

        if ($adapterName === S3FileSystemAdapter::class) {
            $s3Client = $container->get(InfrastructureServicePluginManager::class)->get(
                S3ClientInterface::class,
                $options['adapter']['options']['s3Client'] ?? []
            );

            $adapter = new $adapterName(
                $s3Client
            );
        } else {
            $adapter = new $adapterName(
                $options['adapter']['options']
            );
        }

        return new $requestedName(
            $container->get(InfrastructureServicePluginManager::class),
            $adapter,
            $options ?? []
        );
    }
}
