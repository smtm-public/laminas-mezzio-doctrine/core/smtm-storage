<?php

declare(strict_types=1);

namespace Smtm\Storage\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface StorageServiceAwareInterface
{
    public function getStorageService(): StorageService;
    public function setStorageService(StorageService $storageService): static;
}
